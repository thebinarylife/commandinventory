/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory;

import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

import co.binarylife.commandinventory.commands.CommandManager;
import co.binarylife.commandinventory.data.CIConfig;
import co.binarylife.commandinventory.data.CIInventoryConfig;
import co.binarylife.commandinventory.inventory.InventoryManager;
import co.binarylife.commandinventory.listeners.CommandItemClick;

public class CommandInventory extends JavaPlugin
{
	private static CommandInventory instance;
	
	private InventoryManager im;
	
	private CIConfig config;
	private CIInventoryConfig invConfig;
	
	public void onLoad()
	{
		instance = this;
	}
	
	public static CommandInventory getInstance()
	{
		return instance;
	}
	
	public void onEnable()
	{
		Logger logger = this.getLogger();
		
		logger.info("Loading CommandInventory");
		logger.info("Loading Inventories Config...");
		invConfig = new CIInventoryConfig();
		logger.info("Config loaded");
		
		logger.info("Loading inventories...");
		im = new InventoryManager();
		logger.info("Inventories loaded");
		
		logger.info("Loading commands...");
		loadCommands();
		logger.info("Commands loaded");
		
		logger.info("Loading events...");
		getServer().getPluginManager().registerEvents(new CommandItemClick(im), this);
		logger.info("Events loaded");
		
		logger.info("CommandInventory Loaded");
	}
	
	public void onDisable()
	{
		im.saveInventories();
	}
	
	private void loadCommands()
	{
		CommandManager cm = new CommandManager(im);
		getCommand("ci").setExecutor(cm);
		getCommand("commandinventory").setExecutor(cm);
	}
	
	public CIConfig getPluginConfig()
	{
		return config;
	}
	
	public CIInventoryConfig getInventoriesConfig()
	{
		return invConfig;
	}
	

}
