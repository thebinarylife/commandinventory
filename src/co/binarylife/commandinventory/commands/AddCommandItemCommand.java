/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.commands;

import org.bukkit.entity.Player;

import co.binarylife.commandinventory.inventory.CICommandItem.CommandType;
import co.binarylife.commandinventory.inventory.CIInventory;
import co.binarylife.commandinventory.inventory.InventoryManager;
import co.binarylife.commandinventory.util.NumUtil;
import co.binarylife.commandinventory.util.StringUtil;
import net.md_5.bungee.api.ChatColor;

public class AddCommandItemCommand extends CICommand<Player>
{
	private InventoryManager im;
	
	public AddCommandItemCommand(InventoryManager im)
	{
		super("addcommanditem", "<ID> <slot> <player : console> <command>", "Add a command item to the specified inventory and slot", "commandinventory.addcommanditem", Player.class, 4);
		
		this.im = im;
	}
	
	@Override
	public void run(String[] args, Player player)
	{
		CIInventory inventory = im.getInventory(args[0]);
		if(inventory == null)
		{
			player.sendMessage(StringUtil.PREFIX + ChatColor.RED + "That inventory does not exist!");
			return;
		}

		if(!NumUtil.isInt(args[1]))
		{
			player.sendMessage(StringUtil.PREFIX + ChatColor.RED + getUsage());
			return;
		}
		
		int slot = NumUtil.toInteger(args[1]);
		if(!inventory.hasSlot(slot))
		{
			player.sendMessage(StringUtil.PREFIX + ChatColor.RED + "That slot does not exist in the specified inventory!");
			return;
		}
		
		if(!args[2].equalsIgnoreCase("player") && !args[2].equalsIgnoreCase("console"))
		{
			System.out.println(true);
			player.sendMessage(StringUtil.PREFIX + ChatColor.RED + getUsage());
			return;
		}
		
		inventory.setItem(slot, player.getInventory().getItemInMainHand(), formatCommand(args), CommandType.toCommandType(args[2]));
		player.sendMessage(StringUtil.PREFIX + StringUtil.SECONDARY_COLOR + "The item was added to the inventory!");
	}
	
	private String formatCommand(String[] args)
	{
		String formattedCommand = "";
		for(int i = 3; i < args.length; i++)
		{
			formattedCommand += args[i] + " ";
			System.out.println(args[i]);
		}
		
		return formattedCommand.trim();
	}

}
