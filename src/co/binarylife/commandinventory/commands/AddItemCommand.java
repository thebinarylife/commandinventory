/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.commands;

import org.bukkit.entity.Player;

import co.binarylife.commandinventory.inventory.CIInventory;
import co.binarylife.commandinventory.inventory.InventoryManager;
import co.binarylife.commandinventory.util.NumUtil;
import co.binarylife.commandinventory.util.StringUtil;
import net.md_5.bungee.api.ChatColor;

public class AddItemCommand extends CICommand<Player>
{
	private InventoryManager im;
	
	public AddItemCommand(InventoryManager im)
	{
		super("additem", "<ID> <slot>", "Add item in hand to the specifed inventory in desired slot", "commandinventory.additem", Player.class, 2);
		this.im = im;
	}
	
	public void run(String[] args, Player player)
	{
		CIInventory inventory = im.getInventory(args[0]);
		if(inventory == null)
		{
			player.sendMessage(StringUtil.PREFIX + ChatColor.RED + "That inventory does not exist!");
			return;
		}
		
		if(!NumUtil.isInt(args[1]))
		{
			player.sendMessage(StringUtil.PREFIX + ChatColor.RED + getUsage());
			return;
		}
		
		int slot = NumUtil.toInteger(args[1]);
		if(!inventory.hasSlot(slot))
		{
			player.sendMessage(StringUtil.PREFIX + ChatColor.RED + "That slot does not exist in the specified inventory!");
			return;
		}
		
		inventory.setItem(slot, player.getInventory().getItemInMainHand());
		player.sendMessage(StringUtil.PREFIX + StringUtil.PRIMARY_COLOR +"The item was added to the inventory!");
	}

}
