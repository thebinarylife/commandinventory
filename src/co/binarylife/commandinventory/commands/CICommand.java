/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.commands;

import org.bukkit.command.CommandSender;

import co.binarylife.commandinventory.util.StringUtil;
import net.md_5.bungee.api.ChatColor;

public abstract class CICommand<T extends CommandSender>
{
	private final Class<T> TYPE;
	
	private String command;
	private int argsLength;
	private String usage;
	private String description;
	private String node;
	
	
	public CICommand(String command, String usage, String description, String node, Class<T> type, int argsLength)
	{
		this.command = command;
		this.argsLength = argsLength;
		this.usage = usage;
		this.description = description;
		this.node = node;
		
		this.TYPE = type;
	}
	
	public int getArgsLength()
	{
		return argsLength;
	}
	
	public String getCommand()
	{
		return command;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public String getName()
	{
		return command;
	}
	
	public Class<T> getType()
	{
		return TYPE;
	}
	
	public String getUsage()
	{
		return "/ci " + command + " " + usage;
	}
	
	public boolean hasPermission(CommandSender sender)
	{
		return sender.hasPermission(node);
	}
	
	@SuppressWarnings("unchecked")
	final void handle(CommandSender sender, String[] args)
	{
		if(hasPermission(sender))
			run(args, (T) sender);
		else
			sender.sendMessage(StringUtil.PREFIX + ChatColor.RED + "You must be a player with permission to execute this command!");
	}
	
	public abstract void run(String[] args, T sender);

}
