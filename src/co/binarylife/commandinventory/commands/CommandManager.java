/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.commands;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import co.binarylife.commandinventory.inventory.InventoryManager;
import co.binarylife.commandinventory.util.StringUtil;
import net.md_5.bungee.api.ChatColor;

public class CommandManager implements CommandExecutor
{
	private ArrayList<CICommand<?>> commands;
	
	public CommandManager(InventoryManager im)
	{
		commands = new ArrayList<CICommand<?>>();
		
		commands.add(new AddItemCommand(im));
		commands.add(new AddCommandItemCommand(im));
		commands.add(new CreateInventoryCommand(im));
		commands.add(new InventoryInfoCommand(im));
		commands.add(new ListInventoriesCommand(im));
		commands.add(new OpenInventoryCommand(im));
		commands.add(new RemoveInventoryCommand(im));
	}
	
	private String[] formatArgs(String[] args)
	{
		String[] formattedArgs = new String[args.length - 1];
		for(int i = 1; i < args.length; i++)
		{
			formattedArgs[i-1] = args[i];
		}
		
		return formattedArgs;
	}
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args)
	{
		if(args.length == 0)
		{
			sender.sendMessage(StringUtil.PREFIX + ChatColor.RED + "/ci <args...>");
			return true;
		}
		
		if(args.length == 0 || args[0].equalsIgnoreCase("help"))
		{
			sender.sendMessage(StringUtil.HEADER);
			sender.sendMessage(StringUtil.PRIMARY_COLOR + "Help" + StringUtil.SECONDARY_COLOR + " - " + StringUtil.TERTIARY_COLOR + "Shows all CommandInventory commands!");
			for(CICommand<?> c : commands)
			{
				sender.sendMessage(StringUtil.PRIMARY_COLOR + c.getCommand() + StringUtil.SECONDARY_COLOR + " - " + StringUtil.TERTIARY_COLOR + c.getDescription());
			}
			
			return true;
		}
		
		CICommand<?> command = null;
		for(CICommand<?> c : commands)
		{
			if(c.getCommand().equalsIgnoreCase(args[0]))
			{
				command = c;
				break;
			}
		}
		
		if(command == null)
		{
			sender.sendMessage(StringUtil.PREFIX + ChatColor.RED + "That command does not exist!");
			return true;
		}

		if(!command.getType().isInstance(sender))
		{
			sender.sendMessage(StringUtil.PREFIX + ChatColor.RED + "You must be a player to use that command!");
			return true;
		}
		
		args = formatArgs(args);
		if(args.length < command.getArgsLength())
		{
			sender.sendMessage(StringUtil.PREFIX + ChatColor.RED + command.getUsage());
			return true;
		}
		
		command.handle(sender, args);
		return true;
	}

}
