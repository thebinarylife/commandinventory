/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.commands;

import org.bukkit.command.CommandSender;

import co.binarylife.commandinventory.inventory.CIInventory;
import co.binarylife.commandinventory.inventory.InventoryManager;
import co.binarylife.commandinventory.util.NumUtil;
import co.binarylife.commandinventory.util.StringUtil;
import net.md_5.bungee.api.ChatColor;

public class CreateInventoryCommand extends CICommand<CommandSender>
{
	private InventoryManager im;
	
	public CreateInventoryCommand(InventoryManager im)
	{
		super("createinventory", "<id> <slots> <display name>", "Creates specified inventory, identifiable via ID.", "commandinventory.addcommanditem", CommandSender.class, 3);
		this.im = im;
	}

	@Override
	public void run(String[] args, CommandSender sender)
	{
		if(im.getInventory(args[0]) != null)
		{
			sender.sendMessage(StringUtil.PREFIX + ChatColor.RED + "An inventory with that ID already exists!");
			return;
		}
		
		if(!NumUtil.isInt(args[1]))
		{
			sender.sendMessage(StringUtil.PREFIX + ChatColor.RED + getUsage());
			return;
		}
		
		int slots = NumUtil.toInteger(args[1]);
		
		im.addInventory(new CIInventory(args[0], StringUtil.formatEndOfArgs(args, 2), slots));
		sender.sendMessage(StringUtil.PREFIX + StringUtil.SECONDARY_COLOR + args[0] + StringUtil.PRIMARY_COLOR + " was created with " + im.getInventory(args[0]).getSlots() + " slots!");
	}

}
