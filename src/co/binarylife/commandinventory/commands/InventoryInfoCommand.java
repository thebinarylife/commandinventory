/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.commands;

import org.bukkit.command.CommandSender;

import co.binarylife.commandinventory.inventory.CIInventory;
import co.binarylife.commandinventory.inventory.InventoryManager;
import co.binarylife.commandinventory.util.StringUtil;

public class InventoryInfoCommand extends CICommand<CommandSender>
{
	private InventoryManager im;
	
	public InventoryInfoCommand(InventoryManager im)
	{
		super("info", "<ID>", "Gets Display Name and slot amount for the specified inventory", "commandinventory.inventoryinfo", CommandSender.class, 1);
		this.im = im;
	}
	
	public void run(String[] args, CommandSender sender)
	{
		CIInventory inventory = im.getInventory(args[0]);
		if(inventory == null)
		{
			sender.sendMessage(StringUtil.PREFIX + "That inventory does not exist!");
			return;
		}
		
		sender.sendMessage("\n\n" + StringUtil.HEADER);
		sender.sendMessage(StringUtil.PRIMARY_COLOR + "Display Name" + StringUtil.SECONDARY_COLOR + ": " + StringUtil.TERTIARY_COLOR + inventory.getDisplayName());
		sender.sendMessage(StringUtil.PRIMARY_COLOR + "Slots" + StringUtil.SECONDARY_COLOR + ": " + StringUtil.TERTIARY_COLOR + inventory.getSlots());
	}

}
