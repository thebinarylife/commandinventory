/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.commands;

import org.bukkit.command.CommandSender;

import co.binarylife.commandinventory.inventory.CIInventory;
import co.binarylife.commandinventory.inventory.InventoryManager;
import co.binarylife.commandinventory.util.StringUtil;

public class ListInventoriesCommand extends CICommand<CommandSender>
{
	private InventoryManager im;
	
	public ListInventoriesCommand(InventoryManager im)
	{
		super("list", "", "List all existing inventories", "commandinventory.listinventories", CommandSender.class, 0 );
		this.im = im;
	}
	
	public void run(String[] args, CommandSender sender)
	{
		sender.sendMessage(StringUtil.HEADER);
		for(CIInventory i : im.getInventories())
		{
			sender.sendMessage(StringUtil.PRIMARY_COLOR + i.getID());
		}
	}

}
