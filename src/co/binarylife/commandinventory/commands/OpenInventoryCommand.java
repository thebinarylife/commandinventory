/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import co.binarylife.commandinventory.CommandInventory;
import co.binarylife.commandinventory.inventory.CIInventory;
import co.binarylife.commandinventory.inventory.InventoryManager;
import co.binarylife.commandinventory.util.StringUtil;
import net.md_5.bungee.api.ChatColor;

public class OpenInventoryCommand extends CICommand<CommandSender>
{
	private InventoryManager im;
	
	public OpenInventoryCommand(InventoryManager im)
	{
		super("openinventory", "<ID> [player]", "Opens specified inventory. If player argument is filled it opens the specified inventory for the specified player", "commandinventory.openinventory", CommandSender.class, 1);
		this.im = im;
	}
	
	public void run(String[] args, CommandSender sender)
	{
		CIInventory inventory = im.getInventory(args[0]);
		if(inventory == null)
		{
			sender.sendMessage(StringUtil.PREFIX + ChatColor.RED + "That inventory does not exist!");
			return;
		}
		
		if(args.length == 1)
		{
			if(!(sender instanceof Player))
			{
				sender.sendMessage(StringUtil.PREFIX + ChatColor.RED + "You must be a player to open an inventory for yourself");
				return;
			}
			
			inventory.openInventory((Player) sender);
			return;
		}
		
		Player player = CommandInventory.getInstance().getServer().getPlayer(args[1]);
		if(player == null)
		{
			sender.sendMessage(StringUtil.PREFIX + ChatColor.RED + "That player is not online!");
			return;
		}
		
		inventory.openInventory(player);
		sender.sendMessage(StringUtil.PREFIX + StringUtil.PRIMARY_COLOR + "The inventory was opened for " + StringUtil.SECONDARY_COLOR + player.getDisplayName() + StringUtil.PRIMARY_COLOR + "!");
	}
	
}
