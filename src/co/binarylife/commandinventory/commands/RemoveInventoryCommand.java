/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.commands;

import org.bukkit.command.CommandSender;

import co.binarylife.commandinventory.inventory.CIInventory;
import co.binarylife.commandinventory.inventory.InventoryManager;
import co.binarylife.commandinventory.util.StringUtil;
import net.md_5.bungee.api.ChatColor;

public class RemoveInventoryCommand extends CICommand<CommandSender>
{
	private InventoryManager im;
	
	public RemoveInventoryCommand(InventoryManager im)
	{
		super("removeinventory", "<ID>", "Remove specified inventory!", "commandinventory.removeinventory", CommandSender.class, 1); 
		
		this.im = im;
	}
	
	@Override
	public void run(String[] args, CommandSender sender)
	{
		CIInventory inventory = im.getInventory(args[0]);
		if(inventory == null)
		{
			sender.sendMessage(StringUtil.PREFIX + ChatColor.RED + "That inventory does not exist!");
			return;
		}
		
		im.removeInventory(inventory);
		sender.sendMessage(StringUtil.PREFIX + StringUtil.SECONDARY_COLOR + inventory.getID() + StringUtil.PRIMARY_COLOR + " has been deleted!");
	}

}
