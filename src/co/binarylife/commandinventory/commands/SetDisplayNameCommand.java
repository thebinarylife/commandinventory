/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.commands;

import org.bukkit.command.CommandSender;

import co.binarylife.commandinventory.inventory.CIInventory;
import co.binarylife.commandinventory.inventory.InventoryManager;
import co.binarylife.commandinventory.util.StringUtil;
import net.md_5.bungee.api.ChatColor;

public class SetDisplayNameCommand extends CICommand<CommandSender>
{
	private InventoryManager im;
	
	public SetDisplayNameCommand(InventoryManager im)
	{
		super("setdisplayname", "<id> <display name>", "Sets the display name of the specified inventory", "commandinventory.setdisplayname", CommandSender.class, 2);
		this.im = im;
	}
	
	public void run(String[] args, CommandSender sender)
	{
		CIInventory inventory = im.getInventory(args[0]);
		if(inventory == null)
		{
			sender.sendMessage(StringUtil.PREFIX + ChatColor.RED + "That inventory does not exist!");
			return;
		}
		
		String newDisplayName = StringUtil.formatEndOfArgs(args, 1);
		inventory.setDisplayName(newDisplayName);
		sender.sendMessage(StringUtil.PREFIX + StringUtil.SECONDARY_COLOR + inventory.getID() + StringUtil.PRIMARY_COLOR +"'s display name has been set to " + StringUtil.SECONDARY_COLOR + inventory.getDisplayName() + StringUtil.PRIMARY_COLOR + "!" );
	}

}
