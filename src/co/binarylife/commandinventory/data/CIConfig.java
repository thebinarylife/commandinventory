/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.data;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;

import co.binarylife.commandinventory.CommandInventory;

public class CIConfig
{
	
	private static CIConfig instance;
	
	private File configFile;
	private FileConfiguration config;
	
	public CIConfig()
	{
		CommandInventory plugin = CommandInventory.getInstance();
		
		plugin.saveDefaultConfig();
		
		configFile = new File(plugin.getDataFolder(), "config.yml");
		config = plugin.getConfig();
		
		instance = this;
	}

	public FileConfiguration getConfig()
	{
		return config;
	}
	
	public File getFile()
	{
		return configFile;
	}
	
	public static CIConfig getInstance()
	{
		return instance;
	}
	
	public void save()
	{
		try
		{
			config.save(configFile);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
}
