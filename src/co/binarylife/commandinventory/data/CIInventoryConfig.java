/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.data;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import co.binarylife.commandinventory.CommandInventory;

public class CIInventoryConfig
{
	private File configFile;
	private FileConfiguration config;
	
	public CIInventoryConfig()
	{
		CommandInventory ciInstance = CommandInventory.getInstance();
		
		configFile = new File(ciInstance.getDataFolder(), "inventories.yml");
		if(!configFile.exists())
		{
			try
			{
				configFile.createNewFile();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		config = new YamlConfiguration();
		
		try
		{
			config.load(configFile);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public FileConfiguration getConfig()
	{
		return config;
	}
	
	public File getFile()
	{
		return configFile;
	}
	
	public void save()
	{
		try
		{
			config.save(configFile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
