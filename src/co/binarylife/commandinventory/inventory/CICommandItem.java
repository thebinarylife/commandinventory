/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.inventory;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import co.binarylife.commandinventory.CommandInventory;
import co.binarylife.commandinventory.util.StringUtil;

public class CICommandItem extends CIItem
{
	private String command;
	private CommandType commandType;
	
	public CICommandItem(String command, CommandType commandType, ItemStack item, int slot)
	{
		super(item, slot);
		this.command = command;
		this.commandType = commandType;
	}
	
	public void executeCommand(Player player)
	{
		if(commandType.equals(CommandType.PLAYER))
			player.performCommand(StringUtil.replacePlayer(command, player));
		else
		{
			CommandInventory plugin = CommandInventory.getInstance();
			plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), StringUtil.replacePlayer(command, player));
		}
	}
	
	public String getCommand()
	{
		return command;
	}
	
	public CommandType getType()
	{
		return commandType;
	}
	
	public void setCommand(String command)
	{
		this.command = command;
	}
	
	public enum CommandType 
	{
		CONSOLE("Console"),
		PLAYER("Player");
		
		private String type;
		
		CommandType(String type)
		{
			this.type = type;
		}
		
		public String getType()
		{
			return type;
		}
		
		public static CommandType toCommandType(String type)
		{
			if(type.equalsIgnoreCase("console"))
				return CommandType.CONSOLE;
			
			if(type.equalsIgnoreCase("player"))
				return CommandType.PLAYER;
			
			return null;
		}
	}
	
}
