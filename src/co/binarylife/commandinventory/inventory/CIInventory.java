/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.inventory;

import java.util.HashMap;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import co.binarylife.commandinventory.CommandInventory;
import co.binarylife.commandinventory.data.CIInventoryConfig;
import co.binarylife.commandinventory.inventory.CICommandItem.CommandType;
import net.md_5.bungee.api.ChatColor;

public class CIInventory
{
	private String id;
	private String displayName;
	private int slots;
	
	private Inventory inventory;
	private HashMap<Integer, CIItem> items;
	
	public CIInventory(String id, String displayName, int slots)
	{
		this.id = id;
		
		this.displayName = ChatColor.translateAlternateColorCodes('&', displayName);
		this.slots = slots;
		
		this.items = new HashMap<>();
		
		if(slots % 9 != 0)
			slots += (9 - slots % 9);
		
		this.inventory = CommandInventory.getInstance().getServer().createInventory(null, slots, this.displayName);
		
	}
	
	public String getDisplayName()
	{
		return displayName;
	}
	
	public String getID()
	{
		return id;
	}
	
	public Inventory getInventory()
	{
		return inventory;
	}
	
	public CIItem getItem(int slot)
	{
		return items.get(slot);
	}
	
	public int getSlots()
	{
		return slots;
	}
	
	public boolean hasItemInSlot(int slot)
	{
		return items.containsKey(slot);
	}
	
	public boolean hasSlot(int slot)
	{
		return (slot >= 0 && slot < slots);
	}
	
	public void openInventory(Player player)
	{
		player.openInventory(inventory);
	}
	
	@SuppressWarnings("deprecation")
	public void removeItem(int slot)
	{
		inventory.remove(slot);
	}
	 
	public void save()
	{
		CIInventoryConfig invConfig = CommandInventory.getInstance().getInventoriesConfig();
		ConfigurationSection section = invConfig.getConfig().createSection(id);
		
		section.set("display-name", displayName);
		section.set("slots", slots);
		
		for(int i : items.keySet())
		{
			ConfigurationSection item = section.createSection("" + i);
			item.set("stack", items.get(i).getItem());
			
			if(items.get(i) instanceof CICommandItem)
			{
				item.set("command", ((CICommandItem) items.get(i)).getCommand());
				item.set("command-type", ((CICommandItem) items.get(i)).getType().getType());
			}
		}
	}
	
	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}
	
	public void setItem(int slot, ItemStack item)
	{
		items.put(slot, new CIItem(item, slot));
		inventory.setItem(slot, item);
	}
	
	public void setItem(int slot, ItemStack item, String command, CommandType type)
	{
		items.put(slot, new CICommandItem(command, type, item, slot));
		inventory.setItem(slot, item);
	}
	
}
