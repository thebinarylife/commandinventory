/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.inventory;

import org.bukkit.inventory.ItemStack;

public class CIItem
{
	protected ItemStack item;
	protected int slot;
	
	public CIItem(ItemStack item, int slot)
	{
		this.item = item;
		this.slot = slot;
	}
	
	public ItemStack getItem()
	{
		return item;
	}
	
	public int getSlot()
	{
		return slot;
	}

}
