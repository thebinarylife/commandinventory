/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.inventory;

import java.util.ArrayList;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.Inventory;

import co.binarylife.commandinventory.CommandInventory;
import co.binarylife.commandinventory.data.CIInventoryConfig;
import co.binarylife.commandinventory.inventory.CICommandItem.CommandType;
import co.binarylife.commandinventory.util.NumUtil;

public class InventoryManager
{
	private ArrayList<CIInventory> inventories;
	
	public InventoryManager()
	{
		this.inventories = new ArrayList<>();
		
		loadInventories();
	}
	
	public void addInventory(CIInventory inventory)
	{
		inventories.add(inventory);
	}
	
	public boolean contains(String inventory)
	{
		for(CIInventory i : inventories)
		{
			if(i.getID().equalsIgnoreCase(inventory))
				return true;
		}
		
		return false;
	}
	
	public CIInventory getInventory(Inventory inventory)
	{
		for(CIInventory i : inventories)
		{
			if(i.getInventory().equals(inventory))
				return i;
		}
		
		return null;
	}
	
	public CIInventory getInventory(String inventory)
	{
		for(CIInventory i : inventories)
		{
			if(i.getID().equalsIgnoreCase(inventory))
				return i;
		}
		
		return null;
	}
	
	public ArrayList<CIInventory> getInventories()
	{
		return inventories;
	}
	
	public void loadInventories()
	{
		CIInventoryConfig invConfig = CommandInventory.getInstance().getInventoriesConfig();
		ConfigurationSection config = invConfig.getConfig();
		for(String k : config.getKeys(false))
		{
			ConfigurationSection section = config.getConfigurationSection(k);
			CIInventory inv = new CIInventory(k, section.getString("display-name"), section.getInt("slots"));
			
			for(String s : section.getKeys(false))
			{
				if(!NumUtil.isInt(s))
					continue;
				
				ConfigurationSection item = section.getConfigurationSection(s);
				if(item.contains("command-type"))
					inv.setItem(NumUtil.toInteger(s), item.getItemStack("stack"), item.getString("command"), CommandType.toCommandType(item.getString("command-type")));
				else
					inv.setItem(NumUtil.toInteger(s), item.getItemStack("stack"));
				
			}
			
			inventories.add(inv);
		}
	}
	
	public void removeInventory(String name)
	{
		for(CIInventory i : inventories)
		{
			if(i.getID().equalsIgnoreCase(name))
			{
				inventories.remove(i);
				return;
			}
		}
	}
	
	public void removeInventory(CIInventory inventory)
	{
		inventories.remove(inventory);
		CommandInventory.getInstance().getInventoriesConfig().getConfig().set(inventory.getID(), null);
	}
	
	public void saveInventories()
	{
		for(CIInventory i : inventories)
		{
			i.save();
		}
		
		CommandInventory.getInstance().getInventoriesConfig().save();
	}

}
