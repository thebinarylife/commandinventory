/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;

import co.binarylife.commandinventory.inventory.CICommandItem;
import co.binarylife.commandinventory.inventory.CIInventory;
import co.binarylife.commandinventory.inventory.InventoryManager;

public class CommandItemClick implements Listener
{
	private InventoryManager im;
	
	public CommandItemClick(InventoryManager im)
	{
		this.im = im;
	}
	
	@EventHandler
	public void onCommandItemClick(InventoryClickEvent event)
	{
		Player player = (Player) event.getWhoClicked();
		
		CIInventory inventory = im.getInventory(event.getInventory());
		if(inventory == null)
			return;
		
		if(event.getCurrentItem()== null)
			return;
		
		if(event.getCurrentItem().getType().equals(Material.AIR))
			return;
		
		
		event.setCancelled(true);
		event.setResult(Result.DENY);
		player.updateInventory();

		if(!event.getClick().equals(ClickType.LEFT))
			return;
		
		if(!(inventory.getItem(event.getSlot()) instanceof CICommandItem))
			return;
		
		((CICommandItem) inventory.getItem(event.getSlot())).executeCommand(player);
		
	}
	
}
