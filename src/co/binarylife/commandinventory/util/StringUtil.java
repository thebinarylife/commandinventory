/** Copyright BinaryLife Design & Dev. To Present
All rights reserved
*/

package co.binarylife.commandinventory.util;

import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class StringUtil
{
	public static ChatColor PRIMARY_COLOR = ChatColor.GRAY,
			SECONDARY_COLOR = ChatColor.WHITE,
			TERTIARY_COLOR = ChatColor.DARK_GRAY;
	
	public static String PLAYER_HOLDER = "%PLAYER%",
			PREFIX = PRIMARY_COLOR + "Command" + SECONDARY_COLOR + "Inventory" + TERTIARY_COLOR + ">> ",
			HEADER = ChatColor.translateAlternateColorCodes('&', "\n\n&b<&8------&fCommand&7Inventory&8------&b>");
	
	public static String formatEndOfArgs(String[] args)
	{
		String formattedArgs = "";
		for(int i = 0; i < args.length; i++)
		{
			formattedArgs += args[i] + " ";
			System.out.println(args[i]);
		}
		
		return formattedArgs.trim();
	}
	
	public static String formatEndOfArgs(String[] args, int startingElement)
	{
		String formattedArgs = "";
		for(int i = startingElement; i < args.length; i++)
		{
			formattedArgs += args[i] + " ";
			System.out.println(args[i]);
		}
		
		return formattedArgs.trim();
	}
	
	public static String replacePlayer(String string, Player player)
	{
		return string.replace(PLAYER_HOLDER, player.getName());
	}
	
}
